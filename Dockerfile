FROM node:16

WORKDIR /usr/src/app/client

COPY client/package*.json ./

RUN npm install

COPY client/ .

RUN npm run build

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm", "start" ]
